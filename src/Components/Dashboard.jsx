import React from 'react';
import { Layout, Row, Col, Button, Select, Divider, Icon, Tabs } from 'antd';
import './Dashboard.css';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';

const google = window.google;

const TabPane = Tabs.TabPane;

function callback(key) {
	console.log(key);
}

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const config = {
	chart: {
					type: 'column',
					height: 80,
					width: 330
				},
				plotOptions: {
					series: {
						pointPadding: 0,
						groupPadding: 0
					}				
				},
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					title: {
						enabled: true,
						text: '',
						style: {
							fontWeight: 'normal'
						}
					},
					type: '',
					lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',
					labels: {
						enabled: false
					},
					minorTickLength: 0,
					tickLength: 0,
				},
				yAxis: {
					title: {
						text: ''
					},
					lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',
					labels: {
						enabled: false
					},
					minorTickLength: 0,
					tickLength: 0,
					gridLineColor: 'transparent'

				},
				xAxis: {
					title: {
						text: 'Past 24 hr Trend'
					},
					lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',
					labels: {
						enabled: false
					},
					minorTickLength: 0,
					tickLength: 0,
					gridLineColor: 'transparent'

				},
				legend: {
					enabled: false
				},
				tooltip: {
					pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
				},
					
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

const config2 = {
	chart: {
		type: 'column',
		height: 80,
		width: 330
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: 'Past 24 hr Trend'
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [250.0, 0, 106.4, 0, 0, 0, 0, 0, 0, 194.8, 95.6, 114.4, 219.4, 0, 120.8, 92.78, 0, 83.8, 0, 78.9, 92.0, 0, 56.9, 0]
	}]
};

const wind_speed = {
	 chart: {
		type: 'gauge',
		height: 120,
		width: 120
	},
	title: {
		text: ''
	},
	pane: {
		startAngle: -150,
		endAngle: 150,
		background: [{
			backgroundColor: '#fff',
			borderWidth: 0,
			outerRadius: '109%'
		}, {
			backgroundColor: '#fff',
			borderWidth: 1,
			outerRadius: '107%'
		}, {
			// default background
		}, {
			backgroundColor: '#f1f1f1',
			borderWidth: 0,
			outerRadius: '105%',
			innerRadius: '0%'
		}]
	},
	// the value axis
	yAxis: {
		min: 0,
		max: 60,
		minorTickInterval: 'auto',
		minorTickWidth: 1,
		minorTickLength: 10,
		minorTickPosition: 'inside',
		minorTickColor: '#666',
		tickPixelInterval: 30,
		tickWidth: 2,
		tickPosition: 'inside',
		tickLength: 10,
		tickColor: '#666',
		labels: {
			step: 2,
			distance: -18,
			rotation: 'auto',
			style: {
				fontSize: '8px'
			}
		},
		title: {
			text: ''
		},
		plotBands: [{
			from: 0,
			to: 20,
			color: '#55BF3B' // green
		}, {
			from: 20,
			to: 40,
			color: '#DDDF0D' // yellow
		}, {
			from: 40,
			to: 60,
			color: '#DF5353' // red
		}]
	},
	plotOptions: {
		series: {
			dataLabels: {
				enabled: false
			}
		}
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},
	tooltip: {
		enabled: false
	},
	series: [{
		data: [parseFloat(30)]
	}]
};

const wind_direction = {
	chart: {
		type: 'gauge',
		height: 120,
		width: 120
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},

	title: {
		text: ''
	},
	tooltip: {
		enabled: false,
		backgroundColor: '#FCFFC5',
		borderColor: '#fff',
		pointFormat: '{series.name}: <b>{point.y}</b><br/>',
		valueSuffix: ' °'
	},
	 plotOptions: {
		series: {
			dataLabels: {
				enabled: false
			}
		}
	},

	pane: {
		startAngle: 0,
		endAngle: 360,
		background: [{
			borderWidth: 1,
			outerRadius: '50%'
		}, {
			backgroundColor: '#fff',
			borderWidth: 4,
			outerRadius: '10%'
		}, {
			backgroundColor: '#fff',
			borderWidth: 4,
			outerRadius: '10%'
		}, {
			// default background
		}, {
			backgroundColor: '#f1f1f1',
			borderWidth: 0,
			outerRadius: '105%',
			innerRadius: '0%'
		}]
	},
	// the value axis
	yAxis: [{
		title: {
			text: '',
		},
		min: 0,
		max: 360,
		lineColor: '#333',
		offset: -10,
		tickInterval: 0,
		tickWidth: 2,
		tickPosition: 'outside',
		tickLength: 10,
		tickColor: '#333',
		minorTickInterval: 5,
		minorTickWidth: 1,
		minorTickLength: 10,
		minorTickPosition: 'outside',
		minorTickColor: '#666',
		labels: {
			step: 1,
			distance: -12,
			rotation: 'auto'
		},
	}, {
		title: {
			text: '',
		},
		type: 'category',
		categories: ['N', '' , 'NE', '', 'E', '', 'SE', '', 'S', '', 'SW', '' , 'W', '', 'NW', '', 'N'],
		min: 0,
		max: 16,
		lineColor: '#ddd',
		offset: -20,
		tickInterval: 1,
		tickWidth: 1,
		tickPosition: 'outside',
		tickLength: 20, // =50-10
		tickColor: '#ddd',
		minorTickInterval: 1,
		minorTickWidth: 0,
		minorTickLength: 50,
		minorTickPosition: 'inside',
		minorTickColor: '#0f0',
		labels: {
			step: 1,
			distance: 10,
			rotation: 'auto'
		},
		endOnTick: true,
	}, {
		type: 'number',
		title: {
			text: '',
		},
		labels: {
			enabled: false,
		},
		min: 0,
		max: 12,
		lineColor: '#ddd',
		offset: -50,
		tickInterval: 10,
		tickWidth: 0,
		tickPosition: 'inside',
		tickLength: 45,
		tickColor: '#ddd',
		minorTickWidth: 0,
		endOnTick: false,
	}],

	series: [{
		name: 'East',
		data: [parseFloat(45)],
		dial: {
			radius: '60%',
			baseWidth: 10,
			baseLength: '0%',
			rearLength: 0,
			borderWidth: 1,
			borderColor: '#9A0000',
			backgroundColor: '#CC0000',
		}
	}, {
			name: 'West',
		data: [parseFloat(45)],
		dial: {
			radius: '-60%',
			baseWidth: 10,
			baseLength: '0%',
			rearLength: 0,
			borderWidth: 1,
			borderColor: '#1B4684',
			backgroundColor: '#3465A4',
		}
	}]
};

const pressure = {
	chart: {
		type: 'gauge',
		alignTicks: false,
		plotBackgroundColor: null,
		plotBackgroundImage: null,
		plotBorderWidth: 0,
		plotShadow: false,
		height: 120,
		width: 120
	},
	title: {
		text: ''
	},
	pane: {
		startAngle: -150,
		endAngle: 150,
		background: [{
			borderWidth: 1,
			outerRadius: '50%'
		}, {
			backgroundColor: '#fff',
			borderWidth: 4,
			outerRadius: '10%'
		}, {
			backgroundColor: '#fff',
			borderWidth: 4,
			outerRadius: '10%'
		}, {
			// default background
		}, {
			backgroundColor: '#f1f1f1',
			borderWidth: 0,
			outerRadius: '105%',
			innerRadius: '0%'
		}]
	},
	yAxis: [{
		min: 960,
		max: 1160,
		tickPosition: 'outside',
		lineColor: '#933',
		lineWidth: 2,
		minorTickPosition: 'outside',
		tickColor: '#933',
		minorTickColor: '#933',
		tickLength: 5,
		minorTickLength: 5,
		labels: {
			enabled: true,
			distance: 6,
			rotation: 'auto',
			 style: {
				 fontSize: '8px'
			 }
		},
		offset: -12,
		endOnTick: false
	}],
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},
	tooltip: {
		enabled: false
	},
	series: [{
		data: [1000],
		dataLabels: {
			enabled: "true",
			formatter: function () {
				let mmHg = this.y.toFixed(2),
					mBar = this.y.toFixed(2);
				// return '<span style="color:#339">' + mBar + ' ' + that.state.station_data.param_units[pressure_index]+ '</span><br/>' +'<span style="color:#933">' + mmHg + ' mmHg</span>';
			},
			backgroundColor: '#fff'
		}
	}]

};

const live_graph = {
	chart: {
		zoomType: 'x',
		height: (window.innerHeight - 400)
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		type: 'datetime',
		text: 'Time'
	},
	yAxis: {
		title: {
			text: "μg / m3"
		}
	},
	legend: {
		enabled: false
	},
	/*tooltip: {
		pointFormat: (this.state.current_param == 'rain' ? '<span style="color:{point.color}">' + ('Conc. ') + '<b>{point.y}</b> ' + unit + ' / ' + '<b>{point.y}</b> ' + 'mm' + '<br/>' : '<span style="color:{point.color}">' + (this.state.current_param === 'temperature' || this.state.current_param === 'humidity' || this.state.current_param === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>')
	},*/
	plotOptions: {
		area: {
			marker: {
				radius: 0
			},
			lineWidth: 1,
			states: {
				hover: {
					lineWidth: 1
				}
			},
			threshold: null
		}
	},

	series: [{
		type: 'area',
		 data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

const hourly_data = {
	chart: {
		type: 'column',
		height: (window.innerHeight >= 800) ? 200 : 175
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}
		
	},
	title: {
		text: '1 Hour Average Report',
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: 'Time',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0
	},
	yAxis: {
		title: {
			text: "Conc."
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	/*tooltip: {
		pointFormat: ((this.state.current_param == 'rain') ? '<span style="color:{point.color}">' + 'Conc. ' + '<b>{point.y}</b> ' + unit + ' / ' + '<b>{point.mm}</b> ' + 'mm' + '<br/>' : '<span style="color:{point.color}">' + (this.state.current_param === 'temperature' || this.state.current_param === 'humidity' || this.state.current_param === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>')
	},*/
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

const { Content, Footer } = Layout;

const Option = Select.Option;

function handleChange(value) {
	console.log(`selected ${value}`);
}

function handleBlur() {
	console.log('blur');
}

function handleFocus() {
	console.log('focus');
}

/*const children = [];
for (let i = 1; i < 12; i++) {
	children.push(<Option key={'Device - ' + i}>{'Device - ' + i}</Option>);
}*/

class Dashboard extends React.Component {

	constructor(props) {
		super(props);
		/**
		* This sets the initial state for the page.
		* @type {Object}
		*/
		this.state = {};
		/**
		* This is used for bind the all stations data.
		* @type {object}
		*/
		this.map_view = false;
		this.myStyles = [
			{
				"featureType": "administrative",
				"elementType": "geometry",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "labels",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "transit",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			}
		];
		this.locations= [
			{id:'1', name:'Bhubaneswar-1', lat:'20.2972', long:'85.8665'},
			{id:'2', name:'Bhubaneswar-2', lat:'20.3588', long:'85.8333'},
			{id:'3', name:'Bhubaneswar-3', lat:'20.2718', long:'85.8439'},
			{id:'4', name:'Bhubaneswar-4', lat:'20.2525', long:'85.8159'}
		];
	}

	mapView() {
		let that = this;
		that.map_view = true;
		let city_lat = 20.2972, city_long = 85.8665, zoom_lvl=10;

		var mapProp= {
			center:new google.maps.LatLng(parseFloat(city_lat), parseFloat(city_long)),
			zoom:parseInt(zoom_lvl),
			styles: that.myStyles
		};
		/**
		 * This is used for generating google map.
		 */
		if (document.getElementById('mapView')) {
			console.log('hii', mapProp);
			that.map=new google.maps.Map(document.getElementById('mapView'),mapProp);
		}

		console.log('mapProp', mapProp);
	}

	componentDidUpdate() {
		this.mapView();
		console.log('Component Updated.');
		console.log('All Stations: ', this.locations);
		// let google;
		if(this.locations) {
			this.locations.map((point) => {
				var marker_lat_long = new google.maps.LatLng(point.lat, point.long);
				// console.log(point.lat, point.long);
				var marker_icon = '';
				if (point.connection_status === 'online') {
					if(point.aqi_range == 1) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-1.png';
					} else if(point.aqi_range == 2) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-2.png';
					} else if(point.aqi_range == 3) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-3.png';
					} else if(point.aqi_range == 4) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-4.png';
					} else if(point.aqi_range == 5) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-5.png';
					} else {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-6.png';
					}
				} else {
					marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-0.png';
				}

				var marker = new google.maps.Marker({
					position: marker_lat_long,
					map: this.map,
					title: point.name,
					icon: marker_icon
				});
				// console.log('marker_icon', marker);s
				let content = '', circle_class = '';
				let dashboard_map_pointer = new google.maps.InfoWindow();
				google.maps.event.addListener(marker, 'click',function() {
					if (point.connection_status =='online') {
						circle_class = 'online';
					} else {
						circle_class = 'offline';
					}

					content = '<div class="pointer-details">';
					content += '<div class="flex">';
					content += '<div class="circle center '+ circle_class +'"></div>';
					content += '<div class="qr-code">'+point.name+'</div>';
					content += '</div>';
					dashboard_map_pointer.setContent(content);
					dashboard_map_pointer.open(this.map, marker);
				})
				// google.maps.event.addListener(marker,'click',function() {
				// 	that.fetchStationData(point.id);
				// 	that.context.router.transitionTo('/stations/'+point.id);
				// });
			});
		}
	}

	componentDidMount() {
		if (this.refs.chart) {
			let chart = this.refs.chart.getChart();
			chart.series[0].addPoint({x: 10, y: 12});
		}
	}

	render () {
		const { size } = this.state;

		return (
			<div>
				<Layout>
					<Layout>
						<Content className="contains">
							{/*<Breadcrumb style={{ margin: '16px 0' }}>
								<Breadcrumb.Item>User</Breadcrumb.Item>
								<Breadcrumb.Item>Dashboard</Breadcrumb.Item>
							</Breadcrumb>*/}
							<div className="contain">
								<Row className="rows" className="top-0">
									<Col className="city-select" span={10}>
										<Select
											className="select-icon"
											showSearch
											style={{ width: 400 }}
											placeholder="Select a city"
											defaultValue="Bhubaneswar"
											onChange={handleChange}
											onFocus={handleFocus}
											onBlur={handleBlur}
											filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
										>
											<Option value="Bhubaneswar">Bhubaneswar</Option>
											<Option value="Rajkot">Rajkot</Option>
											<Option value="Udaipur">Udaipur</Option>
										</Select>
									</Col>
								</Row>
								<Row className="rows" type="flex" justify="space-around">
									<Col className="cols map-container" span={14}>
										<div className="full-height" id="mapView" />
									</Col>
									<Col className="cols width-100" span={8}>
										<Select
										className="select-icon mobile-hide"
										placeholder="Select a Device"
										defaultValue="Device-1"
										onChange={handleChange}
										style={{ width: "100%" }}
										>
											<Option key="Device-1">Device-1</Option>
											<Option key="Device-2">Device-2</Option>
											<Option key="Device-3">Device-3</Option>
											<Option key="Device-4">Device-4</Option>
											<Option key="Device-5">Device-5</Option>
											<Option key="Device-6">Device-6</Option>
											<Option key="Device-7">Device-7</Option>
											<Option key="Device-8">Device-8</Option>
										</Select>
										<div className="device-details">
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 1</Col>
												<Col span={6}><span className="danger pad">140</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 2</Col>
												<Col span={6}><span className="success pad">20</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 3</Col>
												<Col span={6}><span className="success pad">40</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 4</Col>
												<Col span={6}><span className="warning pad">80</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 5</Col>
												<Col span={6}><span className="warning pad">90</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 6</Col>
												<Col span={6}><span className="success pad">30</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 7</Col>
												<Col span={6}><span className="success pad">30</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 8</Col>
												<Col span={6}><span className="success pad">30</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 9</Col>
												<Col span={6}><span className="success pad">30</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 10</Col>
												<Col span={6}><span className="success pad">30</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
											<Row type="flex" justify="space-around" className="device-row">
												<Col span={6} className="text-left">Device - 11</Col>
												<Col span={6}><span className="success pad">30</span> AQI</Col>
												<Col span={6}><a href="#">View</a></Col>
											</Row>
										</div>
									</Col>
								</Row>
							</div>
						</Content>
					</Layout>
					<Layout className="pad-t-35">
						<Content className="contains">
							<Row>
								<Select
									className="select-icon mobile-show"
									placeholder="Select a Device"
									defaultValue="Device-1"
									onChange={handleChange}
									style={{ width: "100%" }}
									>
										<Option key="Device-1">Device-1</Option>
										<Option key="Device-2">Device-2</Option>
										<Option key="Device-3">Device-3</Option>
										<Option key="Device-4">Device-4</Option>
										<Option key="Device-5">Device-5</Option>
										<Option key="Device-6">Device-6</Option>
										<Option key="Device-7">Device-7</Option>
										<Option key="Device-8">Device-8</Option>
									</Select>
								</Row>
						</Content>
					</Layout>
					<Layout>
						<Content className="contains">
							<div className="contain aqi-details">
								<Row style={{margin: "0 0 20px 0"}}>
									<Col span={12}><span className="head">Device - 1</span></Col>
								</Row>
								<Row type="flex" justify="space-around">
									<Col span={11} className="white top-0">
										<div className="section-head"><span>AQI Trend</span></div>
										<div className="aqi-trend">
											<Row type="flex" justify="center" align="bottom" className="pad-20 pad-lr-10">
												<Col span={4} className="width-22">
													<img src="http://127.0.0.1:8080/aurassure_logo.svg" className="width-90"/>
													</Col>
												<Col span={6} className="center">
													<div className="success aqi-value">67</div>
													<div className="font-16">AQI</div>
												</Col>
												<Col span={10}>
													<div className="success aqi-status">Satisfactory</div>
													<div>Major Pollutant: SO<sub>2</sub></div>
												</Col>
												<Col span={4}>
													
												</Col>
											</Row>
											<Row className="hr24-graph">
												<Col span={17}>
													<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={6}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
											<div className="aqi-scale"><img src="http://127.0.0.1:8080/aqi_scale.svg" /></div>
										</div>
									</Col>
									<Col className="white parameter-trend display-none" span={11}>
										<div className="section-head">
											<span>AQI Parameter Trend</span>
										</div>
										<Row className="aqi-pollutant-graphs">
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">PM <sub>2.5</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
												</div>
												<Col span={19}>
													<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
										</Row>
										<Row className="aqi-pollutant-graphs">
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">CO <sub>2</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value success">67</span>AQI</span>
												</div>
												<Col span={19}>
													<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
										</Row>
										<Row className="aqi-pollutant-graphs">
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">PM <sub>10</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value success">67</span>AQI</span>
												</div>
												<Col span={19}>
													<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
										</Row>
										<Row className="aqi-pollutant-graphs">
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">SO <sub>2</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value warning">95</span>AQI</span>
												</div>
												<Col span={19}>
													<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
										</Row>
									</Col>
								</Row>
							</div>
						</Content>
					</Layout>
					<Layout>
						<Content className="contains">
							<div className="contain aqi-details">
								<Row type="flex" justify="space-around">
									<Col span={11} className="white top-0">
										<div className="section-head"><span>Weather Trend</span></div>
										<Row type="flex" justify="space-around" align="bottom" className="pad-20 pad-bot-40">
											<Col span={11}>
												<Row className="mar-bot-20" type="flex" justify="center" align="center">
													<Col span={4}><img src="http://127.0.0.1:8080/sun.svg"/></Col>
													<Col className="font-24 pad-left-10" span={5}>27</Col>
													<Col className="font-24" span={4}>°C</Col>
												</Row>
												<Row className="back-col" type="flex" justify="space-around">
													<Col className="weather-values" span={5}>
														<div className="weather-value">25.9</div>
														<div>Avg</div>
													</Col>
													<Col className="weather-values" span={5}>
														<div className="weather-value">21.4</div>
														<div>Min</div>
													</Col>
													<Col className="weather-values" span={5}>
														<div className="weather-value">33</div>
														<div>Max</div>
													</Col>
												</Row>
											</Col>
											<Col span={11}>
												<Row className="mar-bot-20" type="flex" justify="center" align="center">
													<Col span={4}><img src="http://127.0.0.1:8080/drop.svg"/></Col>
													<Col className="font-24 pad-left-10 width-41" span={7}>74%</Col>
													<Col className="font-24" span={4}>RH</Col>
												</Row>
												<Row className="back-col" type="flex" justify="space-around">
													<Col className="weather-values" span={5}>
														<div className="weather-value">25.9</div>
														<div>Avg</div>
													</Col>
													<Col className="weather-values" span={5}>
														<div className="weather-value">21.4</div>
														<div>Min</div>
													</Col>
													<Col className="weather-values" span={5}>
														<div className="weather-value">33</div>
														<div>Max</div>
													</Col>
												</Row>
											</Col>
										</Row>
										{/*<Row className="aqi-pollutant-graphs">
											<div className="graph-name">
												<span className="pollutant-name">Hourly</span>
												<span className="graph-aqi">Rain</span>
											</div>
											<Row className="hr24-graph">
												<Col span={19}>
													<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">0.00</div>
														<div className="graph-value">0.00</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">0.25</div>
														<div className="graph-value">0.01</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
										</Row>*/}
									</Col>
									<Col className="white parameter-trend mar-bot-25" span={11}>
										<div className="section-head">
											<span>Wind Parameter Trend</span>
										</div>
										<Row type="flex" justify="space-around" className="pad-bot-40">
											<Col className="center mar-top-20" span={7}>
												<ReactHighcharts config={wind_speed} ref="speed"></ReactHighcharts>
												<div className="para-value">
													<div>30 m/s</div>
													<div className="bold">Wind Speed</div>
												</div>
											</Col>
											<Col className="center mar-top-20" span={7}>
												<ReactHighcharts config={wind_direction} ref="direction"></ReactHighcharts>
												<div className="para-value">
													<div>45 °</div>
													<div className="bold">Wind Direction</div>
												</div>
											</Col>
											<Col className="center mar-top-20" span={7}>
												<ReactHighcharts config={pressure} ref="pressure"></ReactHighcharts>
												<div className="para-value">
													<div>1000 mBar</div>
													<div className="bold">Pressure</div>
												</div>
											</Col>
										</Row>
									</Col>
								</Row>
							</div>
						</Content>
					</Layout>
					<Layout className="display-none">
						<Content className="contains">
							<div className="contain mar-top-35">
								<Row>
									<Tabs onChange={callback} type="card">
										<TabPane tab="SO2" key="1">SO<sub>2</sub></TabPane>
										<TabPane tab="CO" key="2">CO</TabPane>
										<TabPane tab="CO2" key="3">CO<sub>2</sub></TabPane>
										<TabPane tab="Temperature" key="4">Temperature</TabPane>
										<TabPane tab="Rainfall" key="5">Rainfall</TabPane>
									</Tabs>
								</Row>
								<Row className="gas-graph">
									<ReactHighcharts config={live_graph} ref="gas_graph"></ReactHighcharts>
								</Row>
								<Row className="gas-graph">
									<ReactHighcharts config={hourly_data} ref="hourly_data"></ReactHighcharts>
								</Row>
							</div>
						</Content>
						{/*<Footer className="footer">
						Phoenix Robotix Pvt. Ltd.
						</Footer>*/}
					</Layout>
				</Layout>
			</div>
		);
	}
}

export default Dashboard;