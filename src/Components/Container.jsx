import React, { Component } from 'react';
import './Container.css';
import { Link } from 'react-router-dom';
import { Layout, Menu, Icon, Badge, Drawer } from 'antd';
import Dashboard from './Dashboard.jsx'


const { Header, Sider } = Layout;
const SubMenu = Menu.SubMenu;

const MenuItemGroup = Menu.ItemGroup;

class Container extends Component {
	state = {
	collapsed: true,
	visible: false,
	};

	onCollapse = (collapsed) => {
	this.setState({ collapsed });
	};

	showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

	handleClick = (e) => {
		console.log('click ', e);
		this.setState({
			current: e.key,
		});
	}

	render() {
	return (
		<Layout>
			<Sider
				className="mobile-hidden1"
				collapsible
				onCollapse={this.onCollapse} style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0, zIndex: '11' }}
			>
				<div className="imgs">
				<img src="http://127.0.0.1:8080/aurassure_full_logo.svg" />
				</div>
				<Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
				<Menu.Item key="1">
					<Icon type="layout" />
					<span>Dashboard</span>
				</Menu.Item>
				<Menu.Item key="2">
					<Icon type="file-text" />
					<span>Reports</span>
				</Menu.Item>
				<Menu.Item key="3">
					<Icon type="setting" />
					<span>Configure</span>
				</Menu.Item>
				<Menu.Item key="4">
					<Icon type="switcher" />
					<span>Compare</span>
				</Menu.Item>
				<SubMenu
					key="sub1"
					title={<span><Icon type="user" /><span>User</span></span>}
					>
					<Menu.Item key="5">Profile</Menu.Item>
					<Menu.Item key="6">Change Password</Menu.Item>
				</SubMenu>
				<Menu.Item key="7">
					<Icon type="question-circle-o" />
					<span>Help</span>
				</Menu.Item>
				</Menu>
			</Sider>
			<Drawer
          title=""
          placement="left"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
         <Sider
				collapsible
				onCollapse={this.onCollapse} style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0, zIndex: '11' }}
			>
				<div className="imgs">
				<img src="http://127.0.0.1:8080/aurassure_full_logo.svg" />
				</div>
				<Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
				<Menu.Item key="1">
					<Icon type="layout" />
					<span>Dashboard</span>
				</Menu.Item>
				<Menu.Item key="2">
					<Icon type="file-text" />
					<span>Reports</span>
				</Menu.Item>
				<Menu.Item key="3">
					<Icon type="setting" />
					<span>Configure</span>
				</Menu.Item>
				<Menu.Item key="4">
					<Icon type="switcher" />
					<span>Compare</span>
				</Menu.Item>
				<SubMenu
					key="sub1"
					title={<span><Icon type="user" /><span>User</span></span>}
					>
					<Menu.Item key="5">Profile</Menu.Item>
					<Menu.Item key="6">Change Password</Menu.Item>
				</SubMenu>
				<Menu.Item key="7">
					<Icon type="question-circle-o" />
					<span>Help</span>
				</Menu.Item>
				</Menu>
			</Sider>
        </Drawer>
			<Layout>
					<Header className="header mobile-hidden1" style={{ position: 'fixed', zIndex: 10, width: '100%' }}>
					<Menu
					onClick={this.handleClick}
					theme="light"
					mode="horizontal"
					style={{ lineHeight: '64px' }}
					>
						<SubMenu title={<Link to="#"><Icon type="notification" />Notifications<Badge count={2}/></Link>}>
							<Menu.Item key="1">SO2 of CEMS - 1 above warning limit.</Menu.Item>
							<Menu.Item key="2">SO2 of CEMS - 1 above warning limit.</Menu.Item>
						</SubMenu>
						{/*<Menu.Item key="1"><Link to="#"><Icon type="notification" />Notifications<Badge count={5}/></Link></Menu.Item>*/}
						<Menu.Item key="1"><Link to="#"><Icon type="logout" />Logout</Link></Menu.Item>
					</Menu>
				</Header>
				<Header className="header mobile-show1" style={{ position: 'fixed', zIndex: 10, width: '100%' }}>
          <Icon className="menu-icon" type="menu-unfold" onClick={this.showDrawer} />
					<p className="hed-text">Aurassure</p>
					<Menu
					onClick={this.handleClick}
					theme="light"
					mode="horizontal"
					style={{ lineHeight: '64px' }}
					>
						<SubMenu title={<Link to="#"><Icon type="notification" /><Badge dot/></Link>}>
							<Menu.Item key="1">SO2 of CEMS - 1 above warning limit.</Menu.Item>
							<Menu.Item key="2">SO2 of CEMS - 1 above warning limit.</Menu.Item>
						</SubMenu>
						<Menu.Item className="pad-lr-0" key="1"><Link to="#"><Icon type="logout" /></Link></Menu.Item>
					</Menu>
				</Header>
				<Dashboard/>
			</Layout>
		</Layout>
	);
	}
}

export default Container;